package app.sanberuzadre.two_jobs.repositories;

import app.sanberuzadre.two_jobs.entities.repo.Applicant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ApplicantRepository extends JpaRepository<Applicant, Long> {

}
