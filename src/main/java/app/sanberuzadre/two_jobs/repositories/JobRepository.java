package app.sanberuzadre.two_jobs.repositories;

import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.entities.jobs.Job;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobRepository extends JpaRepository<Job, Long> {
    Job findByName(JobType jobType);
}