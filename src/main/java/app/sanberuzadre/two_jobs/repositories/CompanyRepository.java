package app.sanberuzadre.two_jobs.repositories;

import app.sanberuzadre.two_jobs.entities.repo.Company;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompanyRepository extends JpaRepository<Company, Long> {

    Company findCompanyByName(String name);
}
