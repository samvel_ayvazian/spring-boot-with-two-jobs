package app.sanberuzadre.two_jobs.repositories;

import app.sanberuzadre.two_jobs.entities.repo.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {

}
