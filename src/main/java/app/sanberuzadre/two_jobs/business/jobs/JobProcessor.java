package app.sanberuzadre.two_jobs.business.jobs;

public interface JobProcessor {
    JobType getJobType();
    void process();
}
