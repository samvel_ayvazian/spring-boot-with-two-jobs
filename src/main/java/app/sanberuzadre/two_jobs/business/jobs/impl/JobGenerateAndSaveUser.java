package app.sanberuzadre.two_jobs.business.jobs.impl;

import app.sanberuzadre.two_jobs.business.jobs.JobProcessor;
import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.business.service.ApplicantService;
import app.sanberuzadre.two_jobs.entities.repo.Applicant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class JobGenerateAndSaveUser implements JobProcessor {

    @Autowired
    private ApplicantService applicantService;

    @Override
    public JobType getJobType() {
        return JobType.GENERATE_AND_SAVE_USER;
    }

    @Override
    public void process() {
        log.info("JobType={}", getJobType());
        Applicant applicant = applicantService.createAndSaveDefaultApplicant();
        applicantService.saveDefaultApplicantToFile(applicant);
        log.info("applicant={}", applicant);
        log.info("company={}", applicant.getCompany());
        log.info("location={}", applicant.getLocation());
        log.info("eXecuted : JobType={}", getJobType());
    }
}
