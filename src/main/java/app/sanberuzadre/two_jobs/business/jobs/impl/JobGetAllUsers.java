package app.sanberuzadre.two_jobs.business.jobs.impl;

import app.sanberuzadre.two_jobs.business.jobs.JobProcessor;
import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.business.service.ApplicantService;
import app.sanberuzadre.two_jobs.entities.repo.Applicant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class JobGetAllUsers implements JobProcessor {

    @Autowired
    private ApplicantService applicantService;

    @Override
    public JobType getJobType() {
        return JobType.GET_ALL_USERS;
    }

    @Override
    public void process() {
        log.info("JobType={}", getJobType());
        List<Applicant> getAllApplicantsFromSQL = applicantService.getAllApplicantsFromSQL();
        List<Applicant> getAllApplicantsFromHDD = applicantService.getAllApplicantsFromHDD();
        log.info("Number of Applicants (SQL)={}", getAllApplicantsFromSQL.size());
        log.info("Number of Applicants (HDD)={}", getAllApplicantsFromHDD.size());
        log.info("Difference by size={}", Math.abs(getAllApplicantsFromHDD.size() - getAllApplicantsFromSQL.size()));
        log.info("Are they fully equal?: {}", getAllApplicantsFromHDD.equals(getAllApplicantsFromSQL));
        log.info("eXecuted : JobType={}", getJobType());
    }
}
