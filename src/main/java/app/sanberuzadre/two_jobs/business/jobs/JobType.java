package app.sanberuzadre.two_jobs.business.jobs;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

@Getter
@AllArgsConstructor
public enum JobType {

    GENERATE_AND_SAVE_USER("generateAndSaveUser"),
    GET_ALL_USERS("getAllUsers");

    private final String value;

}
