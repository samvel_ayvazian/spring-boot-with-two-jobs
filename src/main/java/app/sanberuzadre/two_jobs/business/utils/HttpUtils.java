package app.sanberuzadre.two_jobs.business.utils;

import app.sanberuzadre.two_jobs.entities.web.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public final class HttpUtils {

    public static <T> ResponseEntity<Response<T>> buildResponseEntity(Response<T> dataDtoResponse) {
        return dataDtoResponse.isSuccess()
                ? ResponseEntity.ok(dataDtoResponse)
                : ResponseEntity.status(HttpStatus.valueOf(dataDtoResponse.getError().getType().getStatusCode()))
                .body(dataDtoResponse);
    }

    public class ErrorResponse extends Response<Void> {
    }
}
