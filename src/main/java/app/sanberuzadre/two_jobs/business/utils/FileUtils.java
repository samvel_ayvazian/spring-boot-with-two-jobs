package app.sanberuzadre.two_jobs.business.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
public final class FileUtils {

    private static final ObjectMapper om = new ObjectMapper();

    public static void toJsonFile(Object o, final String PATH, final String FILENAME) {
        try {
            File jsonFile = new File(PATH, FILENAME);
            areParentDirectoriesExist(jsonFile);
            deleteFileIfExists(PATH, FILENAME, jsonFile);
            createJsonFile(o, jsonFile);
        } catch (IOException e) {
            throw new IllegalArgumentException("Failed to create Json File...");
        }
    }

    private static void createJsonFile(Object o, File jsonFile) throws IOException {
        if (jsonFile.createNewFile()) {
            log.info("Created file | jsonFile={}", jsonFile);
            om.writeValue(jsonFile, o);
        } else {
            log.info("Already exists | jsonFile={}", jsonFile);
        }
    }

    private static void deleteFileIfExists(String PATH, String FILENAME, File jsonFile) throws IOException {
        if (jsonFile.exists()) {
            Path path = Paths.get(PATH + FILENAME);
            Files.deleteIfExists(path);
        }
    }

    private static void areParentDirectoriesExist(File jsonFile) {
        if (!jsonFile.getParentFile().exists()) {
            jsonFile.getParentFile().mkdirs();
        }
    }
}
