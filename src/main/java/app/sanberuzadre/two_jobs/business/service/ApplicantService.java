package app.sanberuzadre.two_jobs.business.service;

import app.sanberuzadre.two_jobs.entities.repo.Applicant;
import app.sanberuzadre.two_jobs.entities.repo.Company;
import app.sanberuzadre.two_jobs.entities.repo.Location;

import java.util.List;

public interface ApplicantService {

    Applicant createAndSaveDefaultApplicant();

    Company getOrCreateAndSaveDefaultCompany();

    Location createAndSaveDefaultLocation();

    void saveDefaultApplicantToFile(Applicant applicant);

    Applicant save(Applicant applicant);

    Company save(Company company);

    Location save(Location location);

    List<Applicant> getAllApplicantsFromSQL();

    List<Applicant> getAllApplicantsFromHDD();
}
