package app.sanberuzadre.two_jobs.business.service;

import app.sanberuzadre.two_jobs.entities.repo.Company;

public interface CompanyService {

    Company getCompanyByName(String name);

    Company save(Company company);
}
