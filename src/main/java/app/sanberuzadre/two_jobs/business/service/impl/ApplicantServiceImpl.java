package app.sanberuzadre.two_jobs.business.service.impl;

import app.sanberuzadre.two_jobs.business.service.ApplicantService;
import app.sanberuzadre.two_jobs.business.service.CompanyService;
import app.sanberuzadre.two_jobs.business.utils.FileUtils;
import app.sanberuzadre.two_jobs.config.YAMLConfig;
import app.sanberuzadre.two_jobs.entities.business.enums.Gender;
import app.sanberuzadre.two_jobs.entities.repo.Applicant;
import app.sanberuzadre.two_jobs.entities.repo.Company;
import app.sanberuzadre.two_jobs.entities.repo.Location;
import app.sanberuzadre.two_jobs.repositories.ApplicantRepository;
import app.sanberuzadre.two_jobs.repositories.LocationRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

import static app.sanberuzadre.two_jobs.entities.business.constants.ApplicantConstants.DEFAULT_DOB;
import static app.sanberuzadre.two_jobs.entities.business.constants.ApplicantConstants.DEFAULT_SKILLS;
import static app.sanberuzadre.two_jobs.entities.business.constants.LocationConstants.DEFAULT_HOUSE;

@Slf4j
@Service
public class ApplicantServiceImpl implements ApplicantService {

    @Autowired
    private ApplicantRepository applicantRepository;
    @Autowired
    private CompanyService companyService;
    @Autowired
    private LocationRepository locationRepository;
    @Autowired
    private YAMLConfig yamlConfig;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private Faker faker;

    @Override
    public Applicant createAndSaveDefaultApplicant() {
        Applicant applicant = new Applicant();
        applicant.setName(faker.name().firstName());
        applicant.setLastName(faker.name().lastName());
        applicant.setGender(Gender.randomGender());
        applicant.setDob(DEFAULT_DOB);
        applicant.setSkills(getSkills());
        Company company = getOrCreateAndSaveDefaultCompany();
        applicant.setLocation(company.getLocation());
        applicant.setCompany(company);
        applicant = save(applicant);
        return applicant;
    }

    @Override
    public Company getOrCreateAndSaveDefaultCompany() {
        String companyName = faker.company().name();
        Company company = companyService.getCompanyByName(companyName);
        if (company == null) {
            return createCompany(companyName);
        }
        return company;
    }

    private Company createCompany(String companyName) {
        Company company = new Company();
        company.setName(companyName);
        company.setTel(faker.phoneNumber().phoneNumber());
        Location location = createAndSaveDefaultLocation();
        company.setLocation(location);
        company = save(company);
        return company;
    }

    @Override
    public Location createAndSaveDefaultLocation() {
        Location location = new Location();
        location.setCity(faker.address().city());
        location.setStreet(faker.address().streetAddress());
        try {
            location.setHouse(Long.parseLong(faker.address().streetAddressNumber()));
        } catch (IllegalArgumentException e) {
            location.setHouse(DEFAULT_HOUSE);
        }
        location = save(location);
        return location;
    }

    @Override
    public void saveDefaultApplicantToFile(Applicant applicant) {
        final String PATH = yamlConfig.getPathToSavedData();
        final String FILENAME = getFilename(applicant);
        FileUtils.toJsonFile(applicant, PATH, FILENAME);
    }

    private String getFilename(Applicant applicant) {
        final String EXTENSION = ".json";
        return applicant.getId() + applicant.getName() + applicant.getLastName() + EXTENSION;
    }

    @Override
    public Applicant save(Applicant applicant) {
        return applicantRepository.save(applicant);
    }

    @Override
    public Company save(Company company) {
        return companyService.save(company);
    }

    @Override
    public Location save(Location location) {
        return locationRepository.save(location);
    }

    @Override
    public List<Applicant> getAllApplicantsFromSQL() {
        return applicantRepository.findAll();
    }

    @Override
    public List<Applicant> getAllApplicantsFromHDD() {
        List<Applicant> applicants = new ArrayList<>();
        File folder = new File(yamlConfig.getPathToSavedData());
        File[] listOfFiles = folder.listFiles();
        if (listOfFiles != null) {
            getApplicantsFromFiles(applicants, listOfFiles);
        }
        return applicants;
    }

    private void getApplicantsFromFiles(List<Applicant> applicants, File[] listOfFiles) {
        for (File file : listOfFiles) {
            try {
                Applicant applicant = objectMapper.readValue(file, Applicant.class);
                applicants.add(applicant);
                log.info("File successfully read={}", file);
                log.info("Applicant={}", applicant);
            } catch (IOException e) {
                log.info("Invalid File={}", file);
            }
        }
    }

    private String getSkills() {
        List<String> skills = Arrays.asList(readAllFromFile().split(" "));
        Collections.shuffle(skills);
        Random random = new Random();
        return skills.stream().limit(random.nextInt(10 - 1 + 1) + 1).collect(Collectors.joining(", "));
    }

    private String readAllFromFile() {
        File languagesFile = new File(yamlConfig.getLanguagesFile());
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(languagesFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        byte[] data = new byte[(int) languagesFile.length()];
        try {
            fis.read(data);
            fis.close();
        } catch (IOException e) {
            return DEFAULT_SKILLS;
        }
        return new String(data, StandardCharsets.UTF_8);
    }
}
