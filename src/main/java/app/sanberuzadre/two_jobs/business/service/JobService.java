package app.sanberuzadre.two_jobs.business.service;

import app.sanberuzadre.two_jobs.business.jobs.JobType;

public interface JobService {

    void start(JobType jobType);

}
