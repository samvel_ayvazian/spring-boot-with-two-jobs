package app.sanberuzadre.two_jobs.business.service.impl;

import app.sanberuzadre.two_jobs.business.jobs.JobProcessor;
import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.business.service.JobService;
import app.sanberuzadre.two_jobs.entities.jobs.Job;
import app.sanberuzadre.two_jobs.repositories.JobRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Service
public class JobServiceImpl implements JobService {

    @Autowired
    private ExecutorService executorService;
    @Autowired
    private JobRepository jobRepository;

    private Map<JobType, JobProcessor> jobs;

    @Autowired
    private void setJobs(List<JobProcessor> jobProcessors) {
        jobs = jobProcessors.stream().collect(Collectors.toMap(JobProcessor::getJobType, Function.identity()));
    }

    @PostConstruct
    private void init() {
        if (!jobs.values().isEmpty()) {
            jobs.values().forEach(this::processJob);
        }
    }

    @Scheduled(initialDelayString = "${spring.job_initial_delay_ms}", fixedRateString = "${spring.job_interval_ms}")
    private void repeatJobs() {
        log.info("Scheduler | repeat jobs...");
        if (!jobs.values().isEmpty()) {
            jobs.values().forEach(this::processJob);
        }
        log.info("Scheduler | finished repeating jobs!");
    }

    private void processJob(JobProcessor jP) {
        Job job = getJobByName(jP.getJobType());
        if (job == null) {
            job = addJob(jP);
        }
        if (job.isActive()) {
            executorService.execute(jP::process);
            if (!job.isRepeatable()) {
                setJobAsInactive(job);
            }
        }
    }

    private Job addJob(JobProcessor jobProcessor) {
        Job job = new Job();
        job.setName(jobProcessor.getJobType());
        job.setActive(true);
        job.setRepeatable(false);
        save(job);
        return job;
    }

    private void setJobAsInactive(Job job) {
        job.setActive(false);
        save(job);
    }

    private Job getJobByName(JobType jobType) {
        return jobRepository.findByName(jobType);
    }

    @Override
    public void start(JobType jobType) {
        JobProcessor job = jobs.get(jobType);
        if (ObjectUtils.isEmpty(job)) {
            log.warn("Job not found type={}", jobType);
        }
        executorService.execute(job::process);
    }

    private void save(Job job) {
        jobRepository.save(job);
    }
}
