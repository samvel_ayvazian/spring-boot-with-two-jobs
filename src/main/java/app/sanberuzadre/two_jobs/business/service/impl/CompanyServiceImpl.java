package app.sanberuzadre.two_jobs.business.service.impl;

import app.sanberuzadre.two_jobs.business.service.CompanyService;
import app.sanberuzadre.two_jobs.entities.repo.Company;
import app.sanberuzadre.two_jobs.repositories.CompanyRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CompanyServiceImpl implements CompanyService {

    @Autowired
    private CompanyRepository companyRepository;

    @Override
    public Company getCompanyByName(String name) {
        return companyRepository.findCompanyByName(name);
    }

    @Override
    public Company save(Company company) {
        return companyRepository.save(company);
    }
}
