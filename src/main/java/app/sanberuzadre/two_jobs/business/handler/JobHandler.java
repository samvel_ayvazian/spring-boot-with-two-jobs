package app.sanberuzadre.two_jobs.business.handler;

import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.entities.web.Response;

public interface JobHandler {

    Response<JobType> processJob(JobType jobType);
}
