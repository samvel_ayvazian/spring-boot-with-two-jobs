package app.sanberuzadre.two_jobs.business.handler.impl;

import app.sanberuzadre.two_jobs.business.handler.JobHandler;
import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.business.service.JobService;
import app.sanberuzadre.two_jobs.entities.web.Response;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class JobHandlerImpl implements JobHandler {

    @Autowired
    private JobService jobService;

    @Override
    public Response<JobType> processJob(JobType jobType) {
        jobService.start(jobType);
        return new Response<>(jobType);
    }
}
