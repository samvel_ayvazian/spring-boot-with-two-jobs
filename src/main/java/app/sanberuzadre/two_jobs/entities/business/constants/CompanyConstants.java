package app.sanberuzadre.two_jobs.entities.business.constants;

public final class CompanyConstants {
    public static final String DEFAULT_NAME = "Onseo";
    public static final String DEFAULT_TEL = "+380123456789";
}
