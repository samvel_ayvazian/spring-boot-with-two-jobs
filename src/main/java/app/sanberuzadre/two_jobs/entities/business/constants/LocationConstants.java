package app.sanberuzadre.two_jobs.entities.business.constants;

public final class LocationConstants {
    public static final String DEFAULT_CITY = "Vinnytsia";
    public static final String DEFAULT_STREET = "600-ritchya";
    public static final long DEFAULT_HOUSE = 17;
}
