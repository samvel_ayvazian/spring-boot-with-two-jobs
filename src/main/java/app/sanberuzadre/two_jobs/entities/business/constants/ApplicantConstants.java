package app.sanberuzadre.two_jobs.entities.business.constants;

import app.sanberuzadre.two_jobs.entities.business.enums.Gender;

public final class ApplicantConstants {
    public static final String DEFAULT_NAME = "Spring";
    public static final String DEFAULT_LAST_NAME = "Boot";
    public static final Gender DEFAULT_GENDER = Gender.MALE;
    public static final long DEFAULT_DOB = System.currentTimeMillis();
    public static final String DEFAULT_SKILLS = "Java, Spring, Hibernate...";
}
