package app.sanberuzadre.two_jobs.entities.repo;

import app.sanberuzadre.two_jobs.entities.business.enums.Gender;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Applicant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long created;
    private long updated;
    private String name;
    private String lastName;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    private long dob;
    @OneToOne
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private Location location;
    @OneToOne
    @JoinColumn(name = "company_id", referencedColumnName = "id")
    private Company company;
    private String skills;

    @PrePersist
    private void create() {
        setCreated(System.currentTimeMillis());
        setUpdated(System.currentTimeMillis());
    }

    @PreUpdate
    private void update() {
        setUpdated(System.currentTimeMillis());
    }
}
