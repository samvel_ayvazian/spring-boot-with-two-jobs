package app.sanberuzadre.two_jobs.entities.repo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long created;
    private long updated;
    private String name;
    @OneToOne
    @JoinColumn(name = "location_id", referencedColumnName = "id")
    private Location location;
    private String tel;

    @PrePersist
    private void create() {
        setCreated(System.currentTimeMillis());
        setUpdated(System.currentTimeMillis());
    }

    @PreUpdate
    private void update() {
        setUpdated(System.currentTimeMillis());
    }
}
