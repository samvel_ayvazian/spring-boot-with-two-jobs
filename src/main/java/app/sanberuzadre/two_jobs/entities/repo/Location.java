package app.sanberuzadre.two_jobs.entities.repo;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Location {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long created;
    private long updated;
    private String city;
    private String street;
    private long house;

    @PrePersist
    private void create() {
        setCreated(System.currentTimeMillis());
        setUpdated(System.currentTimeMillis());
    }

    @PreUpdate
    private void update() {
        setUpdated(System.currentTimeMillis());
    }
}
