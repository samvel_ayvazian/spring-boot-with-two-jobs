package app.sanberuzadre.two_jobs.entities.jobs;

import app.sanberuzadre.two_jobs.business.jobs.JobType;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Job {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private long created;
    private long updated;
    @Enumerated(EnumType.STRING)
    private JobType name;
    private boolean active;
    private boolean repeatable;

    @PrePersist
    private void create() {
        setCreated(System.currentTimeMillis());
        setUpdated(System.currentTimeMillis());
    }

    @PreUpdate
    private void update() {
        setUpdated(System.currentTimeMillis());
    }
}
