package app.sanberuzadre.two_jobs.entities.web.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType {

    DATA_NOT_FOUND("Data not found", 400),
    BAD_DATA("Bad data", 400),
    AUTHENTICATION_ERROR("Authentication error", 401),
    AUTHORIZATION_ERROR("Authorization error", 403),
    ACCESS_DENIED("Access denied", 403),
    CONFLICT("Conflict", 409),
    TOO_MANY_REQUESTS("Too many requests", 429),
    CONNECTION_ERROR("Connection error", 500);

    private final String name;
    private final int statusCode;

}