package app.sanberuzadre.two_jobs.entities.web.enums;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public enum ErrorText {

    // Common
    ERROR_FIELD_MISSING("Missing field"),
    WRONG_ACCESS_TOKEN("Wrong access token"),
    WRONG_ENUM_VALUE("Wrong enum value");

    private final String description;
}
