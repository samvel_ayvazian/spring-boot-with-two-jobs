package app.sanberuzadre.two_jobs.entities.web;

import app.sanberuzadre.two_jobs.entities.web.enums.ErrorType;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Error {

    private ErrorType type;
    private String description;
    private List<ErrorParameter> errorParameters = new ArrayList<>();

    public Error(ErrorType type, String description) {
        this.type = type;
        this.description = description;
    }

    public Error(ErrorType type, String description, ErrorParameter parameter) {
        this.type = type;
        this.description = description;
        this.errorParameters.add(parameter);
    }

    public Error(ErrorType type, String description, ErrorParameter... parameters) {
        this.type = type;
        this.description = description;
        this.errorParameters.addAll(Arrays.asList(parameters));
    }
}