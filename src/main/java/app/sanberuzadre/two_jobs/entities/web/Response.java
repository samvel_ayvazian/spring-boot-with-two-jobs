package app.sanberuzadre.two_jobs.entities.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response<T> {

    public static final Response<Void> EMPTY = new Response<>();
    private final boolean success;
    private final Error error;
    private final T data;

    public Response() {
        this(true, null, null);
    }

    public Response(T data) {
        this(true, null, data);
    }

    public Response(Error error) {
        this(false, error, null);
    }

    private Response(boolean success, Error error, T data) {
        this.success = success;
        this.error = error;
        this.data = data;
    }
}
