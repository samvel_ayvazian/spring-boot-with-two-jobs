package app.sanberuzadre.two_jobs.entities.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErrorParameter {
    private final String name;
    private final String value;
    private String reason;

    public ErrorParameter(String name, String value) {
        this.name = name;
        this.value = value;
    }
}
