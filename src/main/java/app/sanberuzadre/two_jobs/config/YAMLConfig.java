package app.sanberuzadre.two_jobs.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties
@ConfigurationProperties("job-settings")
@Slf4j
public class YAMLConfig {

    @Value("${spring.name}")
    private String name;
    @Value("${spring.job_initial_delay_ms}")
    private String jobInitialDelayMs;
    @Value("${spring.job_interval_ms}")
    private String jobIntervalMs;
    @Value("${spring.path_to_saved_data}")
    private String pathToSavedData;
    @Value("${spring.languages_file}")
    private String languagesFile;

    public String getName() {
        log.info("name={}", name);
        return name;
    }

    public String getJobInitialDelayMs() {
        log.info("jobInitialDelayMs={}", jobInitialDelayMs);
        return jobInitialDelayMs;
    }

    public String getJobIntervalMs() {
        log.info("jobInterval={}", jobIntervalMs);
        return jobIntervalMs;
    }

    public String getPathToSavedData() {
        log.info("pathToSavedData={}", pathToSavedData);
        return pathToSavedData;
    }

    public String getLanguagesFile() {
        log.info("languagesFile={}", languagesFile);
        return languagesFile;
    }
}