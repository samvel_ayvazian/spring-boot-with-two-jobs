package app.sanberuzadre.two_jobs.web.controller;

import app.sanberuzadre.two_jobs.business.handler.JobHandler;
import app.sanberuzadre.two_jobs.business.jobs.JobType;
import app.sanberuzadre.two_jobs.business.utils.HttpUtils;
import app.sanberuzadre.two_jobs.entities.web.Response;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static app.sanberuzadre.two_jobs.business.utils.HttpUtils.buildResponseEntity;

@Slf4j
@RestController
@RequestMapping("/api/two-jobs/")
public class JobController {

    @Autowired
    private JobHandler jobHandler;

    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request", response = HttpUtils.ErrorResponse.class)
    })
    @GetMapping("/generate-applicant")
    @PreAuthorize("permitAll()")
    public ResponseEntity<Response<JobType>> generateApplicant() {
        Response<JobType> dataResponse = jobHandler.processJob(JobType.GENERATE_AND_SAVE_USER);
        return buildResponseEntity(dataResponse);
    }

    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request", response = HttpUtils.ErrorResponse.class)
    })
    @GetMapping("/get-all-applicants")
    @PreAuthorize("permitAll()")
    public ResponseEntity<Response<JobType>> getAllApplicants() {
        Response<JobType> dataResponse = jobHandler.processJob(JobType.GET_ALL_USERS);
        return buildResponseEntity(dataResponse);
    }
}